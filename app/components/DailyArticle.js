import React from 'react';
import Util from '../utils/util';

const imgPath = 'http://127.0.0.1:8011/img/';
export default class DailyArticle extends React.Component{
    constructor() {
        super();
        this.state = {
            data: {}
        }
    }
    getArticle(id) {
        if(id === 0) return;
        Util.ajax.get('news/' + id).
            then(res => {
                res.body = res.body.replace(/src="http/g, 'src="' + imgPath + 'http');
                res.body = res.body.replace(/src="https/g, 'src="' + imgPath + 'https');
                this.setState({
                    data: res
                })
        })
    }
    componentWillReceiveProps(props) {
        this.getArticle(props.articleId);
    }
    render() {
        return (
            <div className='daily-article'>
                <div className="daily-article-title">{this.state.data.title}</div>
                <div className='daily-article-content' dangerouslySetInnerHTML={{__html:this.state.data.body}}></div>
            </div>
        );
    }
}