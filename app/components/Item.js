import React from 'react';

export default class Item extends React.Component{

    handleClick(data) {
        this.props.handleClick(data.id);
    }
    render() {
        return (
            <a className='daily-item' onClick={this.handleClick.bind(this, this.props.data)}>
                {this.props.data.images ? <div className={'daily-img'}><img src={'http://127.0.0.1:8011/img/'+this.props.data.images[0]}/></div> : ''}
                <div className={this.props.data.images ? 'daily-title' : 'daily-title noImg'}>
                    {this.props.data.title}
                </div>
            </a>
        );
    }
}