/*
* @Author: hcl
* @Date:   2018-08-10 10:51:19
* @Last Modified by:   hcl
* @Last Modified time: 2018-08-10 16:18:57
*/
import React from "react";
import ReactDom from "react-dom";
import ZhihuIndex from "./zhihu/ZhihuIndex"

ReactDom.render(
	<ZhihuIndex />,
	document.getElementById('app')
)