let Util = {};
Util.ajax = {
    get: function(url) {
        return fetch('http://127.0.0.1:8010/' + url)
            .then(res => res.json());
    }
};

Util.getTodayTime = function () {
    const date = new Date();
    date.setHours(0);
    date.setMinutes(0);
    date.setSeconds(0);
    date.setMilliseconds(0);
    return date.getTime();
};

Util.prevDay = function (timestamp = (new Date()).getTime()){
    const date = new Date(timestamp);
    const year = date.getFullYear();
    const month = date.getMonth() + 1 < 10 ?
        '0' + (date.getMonth() + 1) : date.getMonth() + 1;
    const day = date.getDate() < 10 ?
        '0' + date.getDate() :
        date.getDate();
    return year + month + day;
};
Util.formatDay = function(date){
    let month = date.substr(4, 2);
    let day = date.substr(6, 2);
    return `${month}月${day}日`;
}
export default Util;