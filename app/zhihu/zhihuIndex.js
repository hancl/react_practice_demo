import React from 'react';
import Util from '../utils/util';
import Item from '../components/Item';
import DailyArticle from '../components/DailyArticle'
import './style.css';

export default class ZhihuIndex extends React.Component{
    constructor() {
        super();
        this.state = {
            themes: [],
            type: 'recommend',
            showThemes: false,
            themeId: '',
            recommendList: [],
            list: [],
            todayTime: Util.getTodayTime(),
            articleId: 0
        }
    };
    componentDidMount() {
        let _this = this;
        //获取主题日报下的子类
        Util.ajax.get('themes')
            .then(data => {_this.setState({
                themes: data.others
            })});
        this.getRecommendList();
    }
    getRecommendList() {
        let _this = this;
        const prevDay = Util.prevDay(this.state.todayTime + 86400000);
        Util.ajax.get('news/before/' + prevDay).then(res => {
            _this.setState({
                recommendList: [..._this.state.recommendList, res]
            })
        })
    }
    handlerRecomment() {
        this.setState({
            type: "recommend"
        })
    }
    handlerShowThemes() {
        this.setState({
            type: "daily",
            showThemes: !this.state.showThemes
        })
    }
    handleToTheme(id) {
        let _this = this;
        //获取中间栏的数据
        Util.ajax.get('theme/' + id).then(res => {
            _this.setState({
                themeId: id,
                list: res.stories
            })
        })
    }
    handleClick(id) {
        this.setState({
            articleId: id
        })
    }
    render() {
        return (
            <div className='daily'>
                <div className='daily-menu'>
                    <div className={this.state.type === "recommend" ? "daily-menu-item on" : "daily-menu-item" }
                         onClick={this.handlerRecomment.bind(this)}>
                        每日推荐
                    </div>
                    <div className={this.state.type === 'daily' ? 'daily-menu-item on' : 'daily-menu-item'}
                         onClick={this.handlerShowThemes.bind(this)}>
                        主题日报
                    </div>
                    <ul style={{'display': this.state.showThemes ? 'block' : 'none'}}>
                        {this.state.themes.map(item => {
                            return (
                                <li>
                                    <a className={item.id === this.state.themeId && this.state.type === 'daily' ? 'on' : ''}
                                        onClick={this.handleToTheme.bind(this, item.id)}>{item.name}</a>
                                </li>
                            )
                        })}
                    </ul>
                </div>
                <div className="daily-list">
                    <div className={this.state.type === 'recommend'? 'show' : 'hidden'}>
                        {this.state.recommendList.map(list => {
                            let recommentList = list.stories.map(item => {
                                return (
                                    <Item handleClick={this.handleClick.bind(this)} data={item}/>
                                )
                            });
                            {/*这里有点小骚的操作*/}
                            return [<div className='daily-date'>{Util.formatDay(list.date)}</div>, ...recommentList]
                        })}
                    </div>
                    <div className={this.state.type === 'daily'? 'show' : 'hidden'}>
                        {this.state.list.map(item => {
                            return (
                                <Item handleClick={this.handleClick.bind(this)} data={item}/>
                            )
                        })}
                    </div>
                </div>
                <DailyArticle articleId={this.state.articleId} />
            </div>
        );
    }
}