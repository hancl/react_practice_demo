/*
* @Author: hcl
* @Date:   2018-08-10 10:40:11
* @Last Modified by:   hcl
* @Last Modified time: 2018-08-10 16:11:59
*/
let HtmlWebpackPlugin = require('html-webpack-plugin');
let MiniCssExtractPlugin = require('mini-css-extract-plugin');
const path = require("path");
module.exports = {
	entry: "./app/index.js",

	output: {
		path: path.resolve(__dirname, "dist"),
		filename: "bundle.js",
	},

	module: {
		rules: [
			{
				test: /\.js$/,
				include: [
					path.resolve(__dirname, "app")
				],
				exclude: /(node_modules|bower_components)/,
				use: {
					loader: "babel-loader",
					options: {
						presets: ["env", "react"]
					}
				}
			},
            {
                test: /\.css$/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                    },

                    'css-loader' ]
            }
		]
	},

	plugins: [
		new HtmlWebpackPlugin({
			title: "react",
			template: path.resolve(__dirname, "index.html"),
			hash: true
		}),
        new MiniCssExtractPlugin({
            filename: "[name].css"
        })
	]
}